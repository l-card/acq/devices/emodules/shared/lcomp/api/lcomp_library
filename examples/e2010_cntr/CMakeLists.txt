cmake_minimum_required(VERSION 2.6)

project(lcomp_e2010_test)

set(SOURCES main.cpp)

find_package(Threads)
include_directories($<TARGET_PROPERTY:lcomp,INTERFACE_INCLUDE_DIRECTORIES>)
#link_directories(${LTRAPI_LIBRARIES_DIR})

add_executable(${PROJECT_NAME} ${HEADERS} ${SOURCES})

target_link_libraries(${PROJECT_NAME} dl ${CMAKE_THREAD_LIBS_INIT})
target_link_directories(${PROJECT_NAME} PRIVATE  ${LCOMP_LIBRARY_DIR})

#install(TARGETS ${PROJECT} DESTINATION ${LTRAPI_INSTALL_EXAMPLES})
