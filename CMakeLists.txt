cmake_minimum_required(VERSION 2.6)

project(lcomp)

option(LCOMP_BUILD_EXAMPLES "Build examples" OFF)

set(LCOMP_VERSION_MAJOR  1)
set(LCOMP_VERSION_MINOR 58)
set(LCOMP_VERSION_PATCH  2)

set(LCOMP_VERSION  "${LCOMP_VERSION_MAJOR}.${LCOMP_VERSION_MINOR}.${LCOMP_VERSION_PATCH}")

set(LCOMP_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/include)
set(LCOMP_LIBRARY_DIR ${CMAKE_CURRENT_BINARY_DIR})



set(SETUP_HEADERS
    include/lcomp/ifc_ldev.h

    include/lcomp/ioctl.h
    include/lcomp/guiddef.h
    include/lcomp/stubs.h


    include/lcomp/pcicmd.h
    include/lcomp/791cmd.h
    include/lcomp/e140cmd.h
    include/lcomp/e440cmd.h
    include/lcomp/e154cmd.h
    include/lcomp/e2010cmd.h
    )

set(SOURCES
    src/stubs_p.h
    src/ldevbase.h
    src/plx.h
    src/791.h
    src/e140.h
    src/e440.h
    src/e154.h
    src/e2010.h

    src/maincomp.cpp
    src/ldevbase.cpp
    src/lguids.cpp
    src/stubs.cpp

    src/plx.cpp
    src/plx_load.cpp
    src/791.cpp
    src/e140.cpp
    src/e440.cpp
    src/e440_load.cpp
    src/e154.cpp
    src/e2010.cpp
    src/e2010_load.cpp
    )



if(NOT LCOMP_INSTALL_LIB_DIR)
    set(LCOMP_INSTALL_LIB_DIR        ${CMAKE_INSTALL_PREFIX}/lib)
endif(NOT LCOMP_INSTALL_LIB_DIR)
if (NOT LCOMP_INSTALL_INCLUDE_DIR)
    set(LCOMP_INSTALL_INCLUDE_DIR    ${CMAKE_INSTALL_PREFIX}/include)
endif(NOT LCOMP_INSTALL_INCLUDE_DIR)
if(NOT LCOMP_INSTALL_DATA_DIR)
    set(LCOMP_INSTALL_DATA_DIR ${CMAKE_INSTALL_PREFIX}/share)
endif(NOT LCOMP_INSTALL_DATA_DIR)
set(LCOMP_INSTALL_FIRMWARE_DIR  ${LCOMP_INSTALL_DATA_DIR}/lcomp/firmware)

set(LCOMP_COMPILE_DEFINITIONS ${LCOMP_COMPILE_DEFINITIONS} LCOMP_INSTALL_FIRMWARE_DIR="${LCOMP_INSTALL_FIRMWARE_DIR}")


#для GCC устанавливаем повышенный уроень предупреждения компилятора
if(CMAKE_COMPILER_IS_GNUCC)
    include(CheckCCompilerFlag)
    set(CHECK_FLAGS
        -Wall -Wformat-security -Wundef -Wshadow -Wpointer-arith -Wcast-align
        -Wwrite-strings  -Wsign-compare -Waggregate-return -Winline -Wno-aggregate-return
        -Werror=return-type
        -Wextra -Winit-self -Wstrict-aliasing -Wfloat-equal
        -Wunsafe-loop-optimizations -Wlogical-op
        -Wno-unused-parameter -Wno-unused-variable)
    foreach(CUR_FLAG ${CHECK_FLAGS})
        # для корректного названия переменной флага заменяем недопустимые
        # символы - и = в названии флага на _
        string(REGEX REPLACE "-|=" "_" FLAG_VARNAME FLAG_${CUR_FLAG}_ENABLE)

        CHECK_C_COMPILER_FLAG(${CUR_FLAG} ${FLAG_VARNAME})
        if(${FLAG_VARNAME})
            set(LCOMP_COMPILE_FLAGS "${LCOMP_COMPILE_FLAGS} ${CUR_FLAG}")
        endif()
    endforeach(CUR_FLAG)

    CHECK_C_COMPILER_FLAG(-Wl,--no-undefined FLAG_NO_UNDEFINED)
    if(FLAG_NO_UNDEFINED)
        set(LINK_FLAGS "${LINK_FLAGS} -Wl,--no-undefined")
    else(FLAG_NO_UNDEFINED)
        message(STATUS "LINKER FLAG -Wl,--no-undefined WAS NOT FOUND!")
    endif(FLAG_NO_UNDEFINED)
    CHECK_C_COMPILER_FLAG(-fvisibility=hidden FLAG_FVISIBILITY)
    if (FLAG_FVISIBILITY)
        set(LCOMP_COMPILE_FLAGS "${LCOMP_COMPILE_FLAGS} -fvisibility=hidden")
    endif(FLAG_FVISIBILITY)
endif(CMAKE_COMPILER_IS_GNUCC)

add_library(${PROJECT_NAME} SHARED ${SOURCES} ${SETUP_HEADERS})




if(${CMAKE_VERSION} VERSION_LESS "3.0.0")
    include_directories(${LCOMP_INCLUDE_DIR})
    set_target_properties(${PROJECT_NAME} PROPERTIES COMPILE_DEFINITIONS "${LCOMP_COMPILE_DEFINITIONS}")
else()
    target_include_directories(${PROJECT_NAME} PUBLIC  ${LCOMP_INCLUDE_DIR})
    target_compile_definitions(${PROJECT_NAME} PRIVATE ${LCOMP_COMPILE_DEFINITIONS})    
endif()
set_target_properties(${PROJECT_NAME} PROPERTIES COMPILE_FLAGS "${LCOMP_COMPILE_FLAGS}")


# устанавливаем версии (действует только для .so, для .dll нужно через ресурсы
set_target_properties(${PROJECT_NAME} PROPERTIES VERSION ${LCOMP_VERSION})
set_target_properties(${PROJECT_NAME} PROPERTIES SOVERSION ${LCOMP_VERSION_MAJOR})

source_group("Source" ${PROJECT_SOURCE_DIR}/src)
source_group("Public Headers" ${PROJECT_SOURCE_DIR}/include)


if(LCOMP_BUILD_EXAMPLES)
    add_subdirectory(examples)
    source_group("Examples" ${PROJECT_SOURCE_DIR}/examples)
endif(LCOMP_BUILD_EXAMPLES)




if (UNIX)
    install(TARGETS ${PROJECT_NAME} DESTINATION ${LCOMP_INSTALL_LIB_DIR})
    install(FILES ${SETUP_HEADERS} DESTINATION ${LCOMP_INSTALL_INCLUDE_DIR}/lcomp)
    install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/firmware/
            DESTINATION ${LCOMP_INSTALL_FIRMWARE_DIR})
endif(UNIX)

#цели для сборки пакетов
if(UNIX)
    if(NOT OSC_PROJECT)
        set(OSC_PROJECT  home:l-card)
    endif(NOT OSC_PROJECT)
    set(PACKAGE_NAME     liblcomp)
    set(PACKAGE_VERSION  ${LCOMP_VERSION})
    include(packages/packages.cmake)
endif(UNIX)
