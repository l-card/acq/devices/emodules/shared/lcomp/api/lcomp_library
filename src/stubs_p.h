#ifndef STUBS_PRIVATE_H
#define STUBS_PRIVATE_H

#include "lcomp/stubs.h"

#ifdef _WIN32
    #define AO_t int
#else
    #include <fcntl.h>
    #include <unistd.h>
    #include <sys/ioctl.h>
    #include <sys/mman.h>
    #include <atomic_ops.h>
    #include <atomic_ops.h>
#endif
typedef struct { volatile AO_t counter; } atomic_t;


#ifdef _WIN32

#else
    #if __GNUC__ >= 4
        #define LCOMP_EXPORT(type) __attribute__ ((visibility("default"))) type
    #else
        #define LCOMP_EXPORT(type) type
    #endif
#endif




#ifdef CONFIG_SMP
#define LOCK_PREFIX \
      ".section .smp_locks,\"a\"\n" \
      "  .align 4\n"       \
      "  .long 661f\n" /* address */   \
      ".previous\n"        \
            "661:\n\tlock; "

#else /* ! CONFIG_SMP */
#define LOCK_PREFIX ""
#endif

//=====================================
// this for ARM with helper
//typedef int (__kuser_cmpxchg_t)(int oldval, int newval, volatile int *ptr);
//#define __kuser_cmpxchg (*(__kuser_cmpxchg_t *)0xffff0fc0)
//
//static int atomic_add(volatile int *ptr, int val)
// {
//        int old, _new;
//
//        do {
//                old = *ptr;
//                _new = old + val;
//        } while(__kuser_cmpxchg(old, _new, ptr));
//
//        return _new;
//}



#ifdef LCOMP_LINUX
typedef struct _OVERLAPPED {
    ULONG Internal;
    ULONG InternalHigh;
    union {
        struct {
            ULONG Offset;
            ULONG OffsetHigh;
        };

        PVOID Pointer;
    };

    HANDLE  hEvent;
} OVERLAPPED, *LPOVERLAPPED;
#endif



BOOL LDeviceIoControl(HANDLE hDevice,
                       ULONG dwIoControlCode,
                       LPVOID lpInBuffer,
                       ULONG nInBufferSize,
                       LPVOID lpOutBuffer,
                       ULONG nOutBufferSize,
                       PULONG lpBytesReturned,
                       LPOVERLAPPED lpOverlapped);


#endif // STUBS_PRIVATE_H
